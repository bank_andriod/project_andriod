package com.example.appnew;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class ShowActivity extends AppCompatActivity {
    TextView title,showprice,showbank_no,showdate,showtime;
    ImageView imageView;
    String prikey;
    FirebaseUser user;
    String uid;
    int stock = 0;
    int price = 0;
    String photos;
    String titel;
    String date;
    String time;
    String custId;
    String bank_no;
    String shop_name;
    Bundle bundle;
    private FirebaseAuth mAuth;
    DatabaseReference myRef;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        imageView = (ImageView) findViewById(R.id.imgView);
        title = findViewById(R.id.title);
        showprice = findViewById(R.id.price);
        showbank_no = findViewById(R.id.bank_no);
        showdate= findViewById(R.id.date);
        showtime = findViewById(R.id.time);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
        //appBar.setLogo(R.mipmap.ic_launcher);



        bundle = getIntent().getExtras();
        if (bundle != null) {
            photos = bundle.getString("photos");
            titel = bundle.getString("titel");
            price = bundle.getInt("price");
            bank_no = bundle.getString("bank_no");
            custId = bundle.getString("custId");
            date = bundle.getString("date");
            time = bundle.getString("time");
            time = bundle.getString("time");

            Picasso.get().load(photos)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);
            title.setText(titel);
            showprice.setText("฿ "+price);
            showbank_no.setText("เลชบัญชี : "+bank_no);
            showdate.setText("วันที่ชำระเงิน :"+date);
            showtime.setText("เวลาที่ชำระเงิน : "+time);
            appBar.setTitle(titel);

        }
    }
}
