package com.example.appnew;

public class DataImg {
    public  String owner;
    public  String img;
    public  String titel;
    public  String detail;
    public  long   price;
    public  long   stock;
    public  String keypubilc;
    public  String key;
    public  String person;
    public  String logistics;
    public  String shop_name;
    public boolean isSelected;


    public DataImg(){ }

    public DataImg(String owner, String img, String titel, String detail,long price,long stock,String person,String shop_name){
        this.owner=owner;
        this.img=img;
        this.titel=titel;
        this.detail=detail;
        this.price = price;
        this.stock = stock;
        this.person = person;
        this.shop_name = shop_name;

    }



    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

   /* public DataImg(String owner, String img, String titel, String detail,long price,long stock,String keypubilc){
        this.owner=owner;
        this.img=img;
        this.titel=titel;
        this.detail=detail;
        this.price = price;
        this.stock = stock;
        this.keypubilc=keypubilc;
    }
    */


}
