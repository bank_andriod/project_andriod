package com.example.appnew;


import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class DataOut {
    public String bankno;
    public String confrim;
    public String date;
    public String owner;
    public String time;
    public String value ;

    public DataOut(){ }


    public DataOut(String bankno, String confrim, String date, String owner,String time,String value){
        this.date=date;
        this.time=time;
        this.owner=owner;
        this.bankno=bankno;
        this.value=value;
        this.confrim=confrim;
    }

 /*  public Map<String, String> toMap(){
        HashMap<String, String> result = new HashMap<>();
        result.put("date",sDate);
        result.put("time",sTime);
        result.put("owner",sOwner);
        result.put("bankno",sBankNo);
        result.put("value",sValue);
        result.put("confrim",sConfrim);
        return result;
    }*/


}
