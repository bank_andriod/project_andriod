package com.example.appnew.ui.orderfragment2;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.appnew.DataImg;
import com.example.appnew.FragmentCallback;
import com.example.appnew.MyAdapterOrderCust;
import com.example.appnew.MyAdpterTacking;
import com.example.appnew.Order;
import com.example.appnew.OrderActivity;
import com.example.appnew.R;
import com.example.appnew.ui.MyAdapterOrderCust2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class OrderFragment2 extends Fragment {

    private MyAdapterOrderCust2 mAdapter;
    private RecyclerView recyclerView;
    private List<DataImg> datas = new ArrayList<>();
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference myRef2;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    ChildEventListener childEventListener;
    String uid="";
    long sum = 0;

    private OrderFragment2ViewModel mViewModel;

    public static OrderFragment2 newInstance() {
        return new OrderFragment2();
    }


    FragmentCallback mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public OrderFragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_fragment2_fragment, container, false);

        if(savedInstanceState == null) {
            // Inflate the layout for this fragment
            recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setHasFixedSize(true);
            mAdapter = new MyAdapterOrderCust2(datas);
            recyclerView.setAdapter(mAdapter);

            mAuth = FirebaseAuth.getInstance();
            database = FirebaseDatabase.getInstance();

            user = mAuth.getCurrentUser();
            if (user != null) {
                uid = user.getUid();
            }

            getdata();

        }

        //view.findViewById(R.id.btnNextPage).setOnClickListener(this);

        return view;

    }


    public void  getdata(){

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        String keyImg = dataSnapshot.getValue(String.class);
                        myRef2 = database.getReference("imgpubilc");
                        myRef2.child(keyImg).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot2) {
                                DataImg model2 = dataSnapshot2.getValue(DataImg.class);
                                model2.key = dataSnapshot2.getKey();
                                datas.add(model2);
                                recyclerView.scrollToPosition(datas.size() - 1);
                                mAdapter.notifyItemInserted(datas.size() - 1);
                                Log.d("imgdata", dataSnapshot2.toString());

                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {


                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {



                    } catch (Exception ex) {

                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef = database.getReference("basket");
        myRef.child(uid).addChildEventListener(childEventListener);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's state here
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyAdapterOrderCust2) mAdapter).setOnItemClickListener(
                new MyAdapterOrderCust2.MyClickListener() {
                    @Override
                    public void onItemSelected(int position, View v) {

                       sum +=  datas.get(position).price;
                       Toast.makeText(getContext(),""+sum,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onItemNotSelected(int position, View v) {

                        sum -= datas.get(position).price;
                        Toast.makeText(getContext(),""+sum,Toast.LENGTH_LONG).show();

                    }
                }
        );
    }


}