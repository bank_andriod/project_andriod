package com.example.appnew;

public interface MessageListener2 {
    /**
     * To call this method when new message received and send back
     * @param message Message
     */
    void messageReceivedServer(String messageBody);

}
