package com.example.appnew;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class FirebaseViewHolder extends RecyclerView.ViewHolder{
    TextView title, desc,value,confirm;
    public FirebaseViewHolder(View itemView) {
        super(itemView);

        title = (TextView)itemView.findViewById(R.id.txtTitle);
        desc = (TextView)itemView.findViewById(R.id.txtDescription);
        value = (TextView)itemView.findViewById(R.id.textValue);
        confirm = (TextView)itemView.findViewById(R.id.textConfrim);

        //listener set on ENTIRE ROW, you may set on individual components within a row.
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, getAdapterPosition());

            }
        });
        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mClickListener.onItemLongClick(v, getAdapterPosition());
                return true;
            }
        });

    }
    private FirebaseViewHolder.ClickListener mClickListener;

    //Interface to send callbacks...
    public interface ClickListener{
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    public void setOnClickListener(FirebaseViewHolder.ClickListener clickListener){
        mClickListener = clickListener;
    }
}


