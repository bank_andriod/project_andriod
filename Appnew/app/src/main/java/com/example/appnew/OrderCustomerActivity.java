package com.example.appnew;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class OrderCustomerActivity extends AppCompatActivity {
    private MyAdapter mAdapter;
    private RecyclerView recyclerView;
    private List<Data> datas = new ArrayList<>();
    DatabaseReference myRef;
    ChildEventListener childEventListener;
    private FirebaseAuth mAuth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_customer);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
        appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("MyToolbar");

        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("order_customer");
        user = mAuth.getCurrentUser();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(datas);
        recyclerView.setAdapter(mAdapter);
        getdata();
    }

    public void getdata() {

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        datas.add(model);
                        //recyclerView.scrollToPosition(datas.size() - 1);
                        mAdapter.notifyItemInserted(datas.size() - 1);
                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.set(i, model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i, model);
                            }
                        }

                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }

                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef.child(user.getUid()).addChildEventListener(childEventListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }
}
