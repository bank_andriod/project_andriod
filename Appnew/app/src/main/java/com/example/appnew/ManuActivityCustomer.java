package com.example.appnew;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class ManuActivityCustomer extends AppCompatActivity {

    private TextView mTextMessage;
    DatabaseReference myRef;
    ChildEventListener childEventListener;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    String uid;
    private MyAdapterImg mAdapter;
    private RecyclerView recyclerView;
    private List<DataImg> datas = new ArrayList<>();
    ImageView imageView;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return true;
                case R.id.navigation_dashboard:
                    Intent intent = new Intent(getBaseContext(), OrderActivity.class);
                    startActivity(intent);

                    return true;
                case R.id.navigation_notifications:


                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manu_customer);


        mTextMessage = (TextView) findViewById(R.id.message);
        imageView =  (ImageView) findViewById(R.id.thumbnail);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
       // appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("shopping");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        myRef = database.getReference("imgpubilc");

        user = mAuth.getCurrentUser();

        if(user != null) {

            uid = user.getUid();
        }else {
          Toast.makeText( ManuActivityCustomer.this, "Please Sing In",Toast.LENGTH_LONG).show();
          Intent intent = new Intent( ManuActivityCustomer.this, LoginActivity.class);
          startActivity(intent);
          finish();
        }

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapterImg(datas);
        recyclerView.setAdapter(mAdapter);
        getdata();
    }


    public void  getdata(){

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();
                        datas.add(model);
                        recyclerView.scrollToPosition(datas.size() - 1);
                        mAdapter.notifyItemInserted(datas.size() - 1);
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.set(i,model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i,model);
                            }
                        }

                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }

                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef.addChildEventListener(childEventListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }



    @Override
    protected void onResume() {
        super.onResume();
        ((MyAdapterImg) mAdapter).setOnItemClickListener(
                new MyAdapterImg.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Intent intent = new Intent(getBaseContext(), SelectActivity.class);
                        intent.putExtra("photos", datas.get(position).img);
                        intent.putExtra("titel", datas.get(position).titel);
                        intent.putExtra("price", (int)datas.get(position).price);
                        intent.putExtra("stock", (int)datas.get(position).stock);
                        intent.putExtra("detail", datas.get(position).detail);
                        intent.putExtra("shopid", datas.get(position).owner);
                        intent.putExtra("shop_name", datas.get(position).shop_name);
                        intent.putExtra("key", datas.get(position).key);
                        startActivity(intent);

                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 1, 1, "ร้านค้า");
        menu.add(0, 2, 2, "Tracking");
        menu.add(0, 3, 3, "ออกจากระบบ");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case 0:
                Toast.makeText(this, "sel 0", Toast.LENGTH_SHORT).show();
                return true;
            case 1:
                Intent itn = new Intent(this, ManuActivityCustomer.class);
                startActivity(itn);
                return true;
            case 2:
                Intent intent2 = new Intent(getBaseContext(), TackingActivity.class);
                startActivity(intent2);

                return true;
            case 3:
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(this, "ออกจากระบบเรียบร้อย", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ManuActivityCustomer.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
