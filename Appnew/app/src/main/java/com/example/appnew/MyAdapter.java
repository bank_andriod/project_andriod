package com.example.appnew;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{


    private List<Data> mDataset;
    private MyClickListener mCallback;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout,parent,false);

        ViewHolder dataObjHolder = new ViewHolder(view);
        return dataObjHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(mDataset.get(position).toMap().get("bankno"));
        holder.desc.setText("Date: "+mDataset.get(position).toMap().get("date")+"@Time "+mDataset.get(position).toMap().get("time"));
        holder.value.setText("Value: "+mDataset.get(position).toMap().get("value"));
        holder.confirm.setText("Confrim: "+mDataset.get(position).toMap().get("confrim"));
        //holder.icon.setImageResource(mDataset.get(position).getmIcon());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }




    public void setOnItemClickListener(MyClickListener mCallback){
        this.mCallback = mCallback;
    }

    public MyAdapter(List<Data> myDataset) {
        mDataset = myDataset;
    }

    public interface MyClickListener{
        public void onItemClick(int position, View v);

    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView title, desc,value,confirm;
       /* ImageView icon;*/

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.txtTitle);
            desc = (TextView)itemView.findViewById(R.id.txtDescription);
            value = (TextView)itemView.findViewById(R.id.textValue);
            confirm = (TextView)itemView.findViewById(R.id.textConfrim);
            //icon = (ImageView)itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            mCallback.onItemClick(getAdapterPosition(), v);
        }
    }

}
