package com.example.appnew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.appnew.ui.orderfromcust.OrderFromCustFragment;

public class OrderFromCust extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_from_cust_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, OrderFromCustFragment.newInstance())
                    .commitNow();
        }
    }
}
