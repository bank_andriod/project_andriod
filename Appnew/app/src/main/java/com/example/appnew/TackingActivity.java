package com.example.appnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appnew.ui.listtrcking.ListTrckingFragment;
import com.example.appnew.ui.orderfragment2.OrderFragment2;
import com.example.appnew.ui.tackingframent.TackingFramentFragment;
import com.example.appnew.ui.waittransportfragment2.WaitTransportFragment2;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class TackingActivity extends AppCompatActivity implements TackingFramentFragment.MyFragmentListener2{

    private TextView mTextMessage;
    private MyAdpterTacking mAdapter;
    private RecyclerView recyclerView;
    private List<String> datas = new ArrayList<>();
    TackingFramentFragment Event;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        Fragment selectedFragment = null;

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_wait_transport:
                    selectedFragment = TackingFramentFragment.newInstance();
                    getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, selectedFragment).commit();
                    return true;
                case R.id.navigation_process_transport:
                    selectedFragment = ListTrckingFragment.newInstance();
                    getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, selectedFragment).commit();
                    return true;
                case R.id.navigation_transport_succe:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tacking);

       /* Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
     //   appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("Tracking");*/

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            Event = TackingFramentFragment.newInstance();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentContainer, Event);
            transaction.commit();

        }



       /* recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdpterTacking(datas);
        recyclerView.setAdapter(mAdapter);

        datas.add("dvnd");
        datas.add("dvdgsfdgnd");
        datas.add("dvdsv");
        datas.add("dvdsdcvdvv");

        mAdapter.notifyDataSetChanged();*/

    }


    public class MyTarcking extends AsyncTask<String,Void,String> {

        Context mContext;

        MessageListener2 mListener;

        public MyTarcking( Context mContext) {

            this.mContext=mContext;

        }



        @Override
        protected String doInBackground(String... params) {

            final StringBuilder builder = new StringBuilder();

            try {

                Document doc = Jsoup.connect("https://th.kerryexpress.com/en/track/?track="+params[0]).get();
                //Elements info = doc.select("div[class=\"info\"]");
                Elements links = doc.select("div[class=\"status normaly-waiting\"]");

                try {
                    Elements suseec = doc.select("div[class=\"status piority-success\"]");
                    for (Element suseecs : suseec) {
                        Log.d("html2", suseecs.text());
                        datas.add(suseecs.text());
                    }
                }catch (Exception e)
                {
                    Log.d("error" , e.toString());

                }

               /*  for (Element infos : info) {
                    Log.d("html2" , infos.text());
                    datas.add(infos.text());
                }*/





                for (Element link : links) {
                    Log.d("html2" , link.text());
                    datas.add(link.text());

                }



            } catch (Exception e) {
                Log.d("error" , e.toString());

            }

            return builder.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Log.e("TAG", result); // this is expecting a response code to be sent from your server upon receiving the POST data
            // Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();

            Event.doSomethingByActivity(datas);


        }

        public void bindListner(MessageListener2 listener) {
            mListener = listener;
        }

        public void unBindListener() {
            mListener = null;
        }

    }



    @Override
    public void someEvent(String Tracking) {
        datas.clear();
        new MyTarcking(getBaseContext()).execute(Tracking);

    }



}
