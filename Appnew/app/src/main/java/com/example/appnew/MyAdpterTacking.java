package com.example.appnew;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdpterTacking extends RecyclerView.Adapter<MyAdpterTacking.ViewHolder> {
        private List<String> mDataset;
        private MyAdpterTacking.MyClickListener mCallback;
        @Override
        public MyAdpterTacking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tacking,parent,false);

            MyAdpterTacking.ViewHolder dataObjHolder  = new MyAdpterTacking.ViewHolder(view);
            return dataObjHolder;
        }

        @Override
        public void onBindViewHolder(MyAdpterTacking.ViewHolder holder, int position) {
            holder.title.setText(mDataset.get(position));

        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }




        public void setOnItemClickListener(MyAdpterTacking.MyClickListener mCallback){
            this.mCallback = mCallback;
        }

    public MyAdpterTacking(List<String> myDataset) {
            mDataset = myDataset;
        }

        public interface MyClickListener{
            public void onItemClick(int position, View v);

        }

        public class ViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener{
            TextView title;


            public ViewHolder(View itemView) {
                super(itemView);
                title = (TextView)itemView.findViewById(R.id.name);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {

                mCallback.onItemClick(getAdapterPosition(), v);
            }
        }
}
