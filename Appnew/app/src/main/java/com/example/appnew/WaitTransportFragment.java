package com.example.appnew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.appnew.ui.waittransportfragment2.WaitTransportFragment2;

public class WaitTransportFragment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wait_transport_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, WaitTransportFragment2.newInstance())
                    .commitNow();
        }
    }
}
