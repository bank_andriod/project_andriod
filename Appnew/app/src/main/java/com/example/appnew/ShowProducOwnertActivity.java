package com.example.appnew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.UUID;

public class ShowProducOwnertActivity extends AppCompatActivity {

    EditText title,showprice,showstock,showdetail;
    TextView showPerson;
    ImageView imageView;
    String prikey,prikeyPubilc,photos;
    FirebaseUser user;
    String uid,permission;
    String person,shop_name;
    private FirebaseAuth mAuth;
    FirebaseStorage storage;
    StorageReference storageReference,ref;
    DatabaseReference myRef;
    FirebaseDatabase database;
    Uri filePath;
    boolean uploadIMG = false;
    private static final int PICK_IMAGE_REQUEST = 71;
    public static final String MY_PREFS_NAME = "MyPrefsFile";


    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    chooseImage();
                    return true;
                case R.id.navigation_dashboard:
                    uploadImage();
                    uploadDatabase(photos);

                   /* if(!uploadIMG){
                        uploadDatabase(photos);
                    }else {
                        uploadImage();
                    }*/
                    /*uploadDatabase(photos);*/
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_produc_ownert);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("img");
        user = mAuth.getCurrentUser();


        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation2);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        imageView = (ImageView) findViewById(R.id.imgView);
        title = findViewById(R.id.title);
        showprice = findViewById(R.id.price);
        showstock = findViewById(R.id.stock);
        showdetail= findViewById(R.id.detail);
        showPerson= findViewById(R.id.person);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photos = bundle.getString("photos");
            String titel = bundle.getString("titel");
            int price = bundle.getInt("price");
            int stock = bundle.getInt("stock");
            String detail = bundle.getString("detail");
            person = bundle.getString("person");
            prikey = bundle.getString("key");
            uid = bundle.getString("uid");
            permission = bundle.getString("permission");


            Picasso.get().load(photos)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);

            getShopName();

            title.setText(titel);
            showprice.setText(String.valueOf(price));
            showstock.setText(String.valueOf(stock));
            showdetail.setText(detail);
            showPerson.setText("ผู้โพสต์ :"+person);

        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
                uploadIMG=true;
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }



    private void uploadImage() {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            ref = storageReference.child("photos/" + UUID.randomUUID().toString());
            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        photos = downloadUri.toString();
                        progressDialog.dismiss();
                        Toast.makeText(ShowProducOwnertActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();

                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(ShowProducOwnertActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }




    private void uploadDatabase(String path) {
        int lprice = 0;
        int lstock = 0;
        String  KeyImgPubilc;
        try {

            lstock =Integer.parseInt(showstock.getText().toString());
            lprice =Integer.parseInt(showprice.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(ShowProducOwnertActivity.this, "กรุณาใส่ตัวเลข", Toast.LENGTH_LONG).show();
            return;
        }

        DataImg outPubilc = new DataImg(uid,path,title.getText().toString(),showdetail.getText().toString(),lprice,lstock,user.getEmail(),shop_name);


        myRef = database.getReference("imgpubilc");

        myRef.child(prikey).setValue(outPubilc, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Log.d("firebase insert", "Data saved successfully.");
                }
            }
        });


        DataImg outPrivate = new DataImg(uid,path,title.getText().toString(),showdetail.getText().toString(),lprice,lstock,user.getEmail(),shop_name);

        myRef = database.getReference("img");

        myRef.child(uid).child(prikey).setValue(outPrivate, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {

                    Log.d("firebase insert", "Data saved successfully.");
                    Intent itn2 = new Intent(getBaseContext(), BottonActivity.class);
                    itn2.putExtra("permission", permission);
                    itn2.putExtra("uid", uid);
                    startActivity(itn2);
                }
            }
        });




    }

    public void getuidAndpermission() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid = bundle.getString("uid");
            permission = bundle.getString("permission");
        }

    }



    public void getShopName() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
            shop_name = prefs.getString("shopname", "No name defined");//"No name defined" is the default value.
        }

    }
}
