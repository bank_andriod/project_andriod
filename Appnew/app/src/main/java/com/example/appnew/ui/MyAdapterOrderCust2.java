package com.example.appnew.ui;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appnew.DataImg;
import com.example.appnew.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapterOrderCust2 extends RecyclerView.Adapter<MyAdapterOrderCust2.ViewHolder> {
    private List<DataImg> mDataset;
    private MyAdapterOrderCust2.MyClickListener mCallback;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order2,parent,false);

        ViewHolder dataObjHolder  = new ViewHolder(view);
        return dataObjHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final DataImg bb = mDataset.get(position);

        Picasso.get().load(mDataset.get(position).img)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);

        holder.title.setText(mDataset.get(position).titel);
        holder.price.setText("฿"+mDataset.get(position).price);

        /*holder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.checkBox.setChecked(bb.isSelected);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                bb.setSelected(isChecked);
                Log.d("isChecked", ""+isChecked);
            }
        });*/

        holder.checkBox.setChecked(mDataset.get(position).getSelected());

        // holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(position);

        final ViewHolder finalHolder = holder;
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer pos = (Integer) finalHolder.checkBox.getTag();
                // Toast.makeText(ctx, imageModelArrayList.get(pos).getAnimal() + " clicked!", Toast.LENGTH_SHORT).show();

                if (mDataset.get(pos).getSelected()) {
                    mDataset.get(pos).setSelected(false);
                    mCallback.onItemNotSelected(pos,v);
                } else {
                    mDataset.get(pos).setSelected(true);
                    mCallback.onItemSelected(pos, v);
                }
            }
        });







    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }




    public void setOnItemClickListener(MyAdapterOrderCust2.MyClickListener mCallback){
        this.mCallback = mCallback;
    }

    public MyAdapterOrderCust2(List<DataImg> myDataset) {
        mDataset = myDataset;
    }

    public interface MyClickListener{
        public void onItemSelected(int position, View v);
        public void onItemNotSelected(int position, View v);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,price,shop_name;
        ImageView imageView;
        CheckBox checkBox;
        /* ImageView icon;*/

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            price=(TextView)itemView.findViewById(R.id.price);
            imageView = (ImageView) itemView.findViewById(R.id.thumbnail);
            shop_name = (TextView)itemView.findViewById(R.id.shopname);
            checkBox = (CheckBox) itemView.findViewById(R.id.cbSelect);
            //email=(TextView)itemView.findViewById(R.id.email);
            //itemView.setOnClickListener(this);

        }

        /*@Override
        public void onClick(View v) {

            mCallback.onItemClick(getAdapterPosition(), v);
        }*/
    }

}
