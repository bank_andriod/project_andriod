package com.example.appnew;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MySQLConnection extends AsyncTask<String,Void,String> {

     Context ctx;



    public MySQLConnection(Context ctx) {
        this.ctx=ctx;
    }

    @Override
    protected String doInBackground(String... strings) {
        String urlrequest = strings[0];
        String urlparams = strings[1];

        try {
            URL url = new URL(urlrequest);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            DataOutputStream dataOutput = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutput.writeBytes(urlparams);
            dataOutput.flush();
            dataOutput.close();
            DataInputStream dataInput = new DataInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInput));
            String response = bufferedReader.readLine();
            bufferedReader.close();
            httpURLConnection.disconnect();
            return response;
        } catch (MalformedURLException e) {

        } catch (UnsupportedEncodingException e) {

        } catch (IOException e) {

        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);

        String results[] = result.split("");
        if(results[0].equals("250")){
            Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();
        }
//        else if(results[0].equals("550")){
//            Toast.makeText(ctx,"LOGIN ERROR",Toast.LENGTH_LONG).show();
//        }else{
//            Toast.makeText(ctx," ERROR",Toast.LENGTH_LONG).show();
//
//        }

    }
}

