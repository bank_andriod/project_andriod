package com.example.appnew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.appnew.ui.listtrcking.ListTrckingFragment;

public class ListTrcking extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_trcking_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, ListTrckingFragment.newInstance())
                    .commitNow();
        }
    }
}
