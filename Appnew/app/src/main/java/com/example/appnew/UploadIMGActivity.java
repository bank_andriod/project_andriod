package com.example.appnew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.UUID;

public class UploadIMGActivity extends AppCompatActivity {
    FirebaseStorage storage;
    StorageReference storageReference;
    Uri filePath;
    Uri downloadUri;
    ImageView imageView;
    EditText titel,detail,price,stock;
    Button btnUpload;
    FirebaseUser user;
    String uid,shop_name;
    String permission;
    StorageReference ref;
    private FirebaseAuth mAuth;
    DatabaseReference myRef;
    FirebaseDatabase database;
    private static final int PICK_IMAGE_REQUEST = 71;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_img);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        imageView = (ImageView) findViewById(R.id.imgView);
        titel =(EditText)findViewById(R.id.titletext);
        detail =(EditText)findViewById(R.id.textdetail);
        price =(EditText)findViewById(R.id.textprice);
        stock =(EditText)findViewById(R.id.textstock);
        btnUpload = (Button) findViewById(R.id.btnUpload);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("img");
        user = mAuth.getCurrentUser();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
            shop_name = prefs.getString("shopname", "No name defined");//"No name defined" is the default value.
        }

        getuidAndpermission();

        if (user == null) {

            Toast.makeText(getBaseContext(), "Please Sing In", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

        chooseImage();
    }

    public void getuidAndpermission() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid = bundle.getString("uid");
            permission = bundle.getString("permission");
        }

    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            ref = storageReference.child("photos/"+ UUID.randomUUID().toString());
            ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        uploadDatabase(downloadUri.toString());
                        progressDialog.dismiss();
                        Toast.makeText(UploadIMGActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(UploadIMGActivity.this,BottonActivity.class);
                        startActivity(intent);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(UploadIMGActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

           /* ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //String path = taskSnapshot.getUploadSessionUri().toString();
                            String path = taskSnapshot.getMetadata().getPath();
                            progressDialog.dismiss();
                            uploadDatabase(path);
                            Toast.makeText(UploadIMGActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UploadIMGActivity.this,BottonActivity.class);
                            startActivity(intent);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(UploadIMGActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });*/





        }
    }

    private void uploadDatabase(String path) {
        int lprice = 0;
        int lstock = 0;
        String  KeyImgPubilc;
        try {

            lstock =Integer.parseInt(stock.getText().toString());
            lprice =Integer.parseInt(price.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(UploadIMGActivity.this, "กรุณาใส่ตัวเลข", Toast.LENGTH_LONG).show();
            return;
        }

        DataImg outPubilc = new DataImg(uid,path,titel.getText().toString(),detail.getText().toString(),lprice,lstock,user.getEmail(),shop_name);


        myRef = database.getReference("imgpubilc");

        DatabaseReference newPostRef = myRef.push();
        KeyImgPubilc = newPostRef.getKey();

        newPostRef.setValue(outPubilc, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Log.d("firebase insert", "Data saved successfully.");
                }
            }
        });


        DataImg outPrivate = new DataImg(uid,path,titel.getText().toString(),detail.getText().toString(),lprice,lstock,user.getEmail(),shop_name);

        myRef = database.getReference("img");

        myRef.child(uid).child(KeyImgPubilc).setValue(outPrivate, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Log.d("firebase insert", "Data saved successfully.");
                }
            }
        });


    }



}
