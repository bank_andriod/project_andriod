package com.example.appnew;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class InsertTrakingCustActivity extends AppCompatActivity {

    TextView title,showprice,showbank_no,showdate,showtime;
    EditText Tracking;
    private Button btn_sumit;
    ImageView imageView;
    String prikey;
    FirebaseUser user;
    String uid;
    int stock = 0;
    int price = 0;
    String TrackingText;
    String photos;
    String titel;
    String date;
    String time;
    String custId;
    String bank_no;
    String KerOrder;
    Bundle bundle;
    private FirebaseAuth mAuth;
    DatabaseReference myRef;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_traking_cust);

        imageView = (ImageView) findViewById(R.id.imgView);
        title = findViewById(R.id.title);
        showprice = findViewById(R.id.price);
        showbank_no = findViewById(R.id.bank_no);
        showdate= findViewById(R.id.date);
        showtime = findViewById(R.id.time);
        Tracking = findViewById(R.id.tracking);
        btn_sumit= findViewById(R.id.btn_sumit);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if (user != null) {
            uid = user.getUid();
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
        appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("รายละเอียดสินค้าที่จัดส่ง");


        bundle = getIntent().getExtras();
        if (bundle != null) {
            photos = bundle.getString("photos");
            titel = bundle.getString("titel");
            price = bundle.getInt("price");
            bank_no = bundle.getString("bank_no");
            custId = bundle.getString("custId");
            date = bundle.getString("date");
            time = bundle.getString("time");
            KerOrder = bundle.getString("KerOrder");

            Picasso.get().load(photos)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);
            title.setText(titel);
            showprice.setText("฿ "+price);
            showbank_no.setText("เลชบัญชี : "+bank_no);
            showdate.setText("วันที่ชำระเงิน :"+date);
            showtime.setText("เวลาที่ชำระเงิน : "+time);

        }

        btn_sumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TrackingText = Tracking.getText().toString();
                myRef= database.getReference("order_tracking");

                OrderDetailTrcking data = new OrderDetailTrcking(date,KerOrder,user.getUid(),""+price,custId,bank_no,time,TrackingText);
                myRef.child(user.getUid()).push().setValue(data);
                myRef.child(custId).push().push().setValue(data);
                Toast.makeText(getBaseContext(),"เพิ่มเข้ารายการเรียบร้อย", Toast.LENGTH_LONG).show();
                myRef= database.getReference("order_cash");
                myRef.child(user.getUid()).child(KerOrder).setValue(null);
            }
        });


    }

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_traking_cust);
    }*/
}
