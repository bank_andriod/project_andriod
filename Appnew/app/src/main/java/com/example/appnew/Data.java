package com.example.appnew;


import java.util.HashMap;
import java.util.Map;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Data {
    public String date;
    public String value;
    public String bankno;
    public String time;
    public String owner;
    public String confrim;
    public String key ;

    public Data(){ }

    public Data(String date, String value, String bankno, String time,String owner,String confrim,String key){
        this.date=date;
        this.time=time;
        this.owner=owner;
        this.bankno=bankno;
        this.value=value;
        this.confrim=confrim;
        this.key=key;
        }

   public Map<String, String> toMap(){
        HashMap<String, String> result = new HashMap<>();
        result.put("date",date);
        result.put("time",time);
        result.put("owner",owner);
        result.put("bankno",bankno);
        result.put("value",value);
        result.put("confrim",confrim);
        result.put("key",key);
        return result;
    }


}
