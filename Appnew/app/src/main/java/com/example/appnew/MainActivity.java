package com.example.appnew;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int SMS_PERMISSION_CODE = 101;
    private static final int PICK_IMAGE_REQUEST = 71;
    SmsListener msg;
    private MyAdapter mAdapter;
    private RecyclerView recyclerView;
    private List<Data> datas = new ArrayList<>();
    String uid = "WecALOlR88PwlHpNL5fkahplDer1";
    String sms = "15-03-19@13:30 บช X26007X:เงินออก -5,500.00 บ. ใช้ได้ 2,898.46 บ.";
    DatabaseReference myRef;
    ChildEventListener childEventListener;
    private FirebaseAuth mAuth;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("user");
        user = mAuth.getCurrentUser();

        Bundle bundle = getIntent().getExtras();
        if (bundle == null ) {

            if(user != null)
            {
                RouterPermission();

            }else {

                Toast.makeText(MainActivity.this, "Please Sing In", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }else {
            if (user != null) {
                uid = user.getUid();
                myRef = database.getReference("revenue");
            }
        }

        /*

        String[] Body =sms.split(" ");

        String[] date =Body[0].split("@");

        String[] bankno =Body[2].split(":");

        String[] buffer = Body[3].split("-");
        String res ="";
       // String[] buffer2 =buffer[1].split(".");

        //Toast.makeText(MainActivity.this, buffer2[0],Toast.LENGTH_LONG).show();


       if(Body[3].substring(0,1).equals("-")){

           try {

               String[] buffer3 = Body[3].substring(1,Body[3].length()-3).split(",");
                res = buffer3[0]+buffer3[1];

           }catch (Exception e)
           {
                res = Body[3].substring(1,Body[3].length()-3);
           }

           int i =Integer.parseInt(res);

           Toast.makeText(MainActivity.this, date[0]+date[1]+bankno[0]+""+i,Toast.LENGTH_LONG).show();

        }
*/

        // getSupportActionBar().hide();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
        appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("MyToolbar");


        ChackPermissions();




        msg = new SmsListener();
        //registerReceiver(msg, new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION));
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(Telephony.Sms.Intents.DATA_SMS_RECEIVED_ACTION);
        this.registerReceiver(msg, intentFilter);



        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(datas);
        recyclerView.setAdapter(mAdapter);
        getdata();


        /*DataOut out= new DataOut("X59884","no","07-01-09","eferfdgvre","00:48","5888");
        myRef.child(uid).push().setValue(out);*/

        /*myRef.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    for ( DataSnapshot userdata : dataSnapshot.getChildren()) {
                        Data datain = userdata.getValue(Data.class);
                        datain.key=   userdata.getKey();
                        datas.add(datain);
                        Log.d("User data" , userdata.toString());
                    }

                }

                mAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(DatabaseError error) {

                Toast.makeText(MainActivity.this,""+error,Toast.LENGTH_LONG).show();

            }
        });*/

    }

    private void  RouterPermission(){

        myRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserManager value = dataSnapshot.getValue(UserManager.class);
                if(value.permission.contains("customer")){

                    Intent intent = new Intent(MainActivity.this, ManuActivityCustomer.class);
                    startActivity(intent);
                    finish();

                }
                else {
                    Intent intent = new Intent(MainActivity.this, BottonActivity.class);

                    intent.putExtra("userPermission", value.permission);

                    startActivity(intent);

                    finish();
                }


            }

            @Override
            public void onCancelled(DatabaseError error) {
                Toast.makeText(MainActivity.this,error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 1, 1, "ร้านค้า");
        menu.add(0, 2, 2, "ดูรายการย้อนหลัง");
        menu.add(0, 3, 3, "ออกจากระบบ");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case 0:
                Toast.makeText(this, "sel 0", Toast.LENGTH_SHORT).show();
                return true;
            case 1:
                Intent itn = new Intent(this, BottonActivity.class);
                startActivity(itn);
                return true;
            case 2:

                // Intent itn = new Intent(this, PageinfoActivity.class);
                //startActivity(itn);
//                Toast.makeText(this,"sel 2",Toast.LENGTH_SHORT).show();
                return true;
            case 3:

                FirebaseAuth.getInstance().signOut();
                Toast.makeText(this, "ออกจากระบบเรียบร้อย", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void ChackPermissions() {

        if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECEIVE_SMS)) != (PackageManager.PERMISSION_GRANTED)) {

            showRequestPermissionsInfoAlertDialog(true);

        } else if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) != (PackageManager.PERMISSION_GRANTED)) {
            showRequestPermissionsInfoAlertDialogIMG();
        }

        return;
    }

    public void showRequestPermissionsInfoAlertDialog(final boolean makeSystemRequest) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, SMS_PERMISSION_CODE);

        } else {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, SMS_PERMISSION_CODE);

        }


    }

    public void showRequestPermissionsInfoAlertDialogIMG() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECEIVE_SMS)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECEIVE_SMS}, PICK_IMAGE_REQUEST);

        } else {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECEIVE_SMS}, PICK_IMAGE_REQUEST);

        }


    }



   /* public void showRequestPermissionsInfoAlertDialog(final boolean makeSystemRequest) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.permission_alert_dialog_title); // Your own title
        builder.setMessage(R.string.permission_dialog_message); // Your own message
      //  builder.setNegativeButton()
        builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Display system runtime permission request?
                if (makeSystemRequest) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.READ_SMS)) {

                        Log.e("onRequestPermission", );

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_SMS},SMS_PERMISSION_CODE);

                    }
                }
            }
        });

        builder.setCancelable(false);
        builder.show();
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SMS_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // SMS related task you need to do.
                    Log.d("onRequestPermission", "" + grantResults[0]);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case PICK_IMAGE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // SMS related task you need to do.
                    Log.d("onRequestPermission", "" + grantResults[0]);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void getdata() {

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        datas.add(model);
                        //recyclerView.scrollToPosition(datas.size() - 1);
                        mAdapter.notifyItemInserted(datas.size() - 1);
                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.set(i, model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i, model);
                            }
                        }

                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Data model = dataSnapshot.getValue(Data.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }

                        Log.d("User data", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef.child(uid).addChildEventListener(childEventListener);

    }


    @Override
    protected void onResume() {
        super.onResume();
        ((MyAdapter) mAdapter).setOnItemClickListener(
                new MyAdapter.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Toast.makeText(getApplication(), "Clicked item " + Integer.toString(position)
                                , Toast.LENGTH_SHORT).show(); // + Integer.toString(position)


                    }
                }
        );

        msg.bindListner(new MessageListener() {
            @Override
            public void messageReceived(String messageBody) {

                String res = "";

                String[] Body = messageBody.split(" ");

                if (Body[3].substring(0, 1).equals("-")) {
                    return;
                }

                String[] date = Body[0].split("@");

                String[] bankno = Body[2].split(":");

                try {

                    String[] buffer = Body[3].substring(1, Body[3].length() - 3).split(",");
                    res = buffer[0] + buffer[1];

                } catch (Exception e) {
                    res = Body[3].substring(1, Body[3].length() - 3);
                }

                DataOut out = new DataOut(bankno[0], "no", date[0]/*date*/, uid, date[1]/*time*/, res);

                myRef.child(uid).push().setValue(out, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                        } else {
                            Log.d("firebase insert", "Data saved successfully.");
                        }
                    }
                });
                Log.d("messageBody", messageBody);
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        msg.unBindListener();
        unregisterReceiver(msg);

    }


}




