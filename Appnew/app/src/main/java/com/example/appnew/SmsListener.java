package com.example.appnew;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsListener extends BroadcastReceiver {
    private static MessageListener mListener;
    private String uid;
    public String smsBody = "";



    public SmsListener() {

    }



    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from ="";
            String smsSender = "";

            Log.d("onTextReceived", ""+bundle);

             if (bundle != null){
                //---retrieve the SMS message received---
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();

                        if (msg_from.equals("Krungthai")) {
                            smsBody = smsBody + msgs[i].getMessageBody();
                            Log.d("Krungthai", smsBody);

                        } else {
                            smsBody = smsBody + msgs[i].getMessageBody();

                        }

                    }
                    Log.d("msgBody", smsBody);
                    if(mListener !=null) {
                        mListener.messageReceived(smsBody);
                    }

                }catch(Exception e){
                           Log.d("Exception caught",e.getMessage());
                }
            }
        }
    }


   /* public  void setListener(final Listener listener) {

            this.listener = listener;


    }

    interface Listener {
        void onTextReceived(String text);
    }*/

    public void bindListner(MessageListener listener){
        mListener =listener;
    }

    public  void unBindListener(){
        mListener=null;
    }
}


