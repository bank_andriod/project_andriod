package com.example.appnew;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyAdpterTacking2 extends RecyclerView.Adapter<MyAdpterTacking2.ViewHolder> {
        private List<String> mDataset;
        private MyAdpterTacking2.MyClickListener mCallback;
        @Override
        public MyAdpterTacking2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tackinglist,parent,false);

            MyAdpterTacking2.ViewHolder dataObjHolder  = new MyAdpterTacking2.ViewHolder(view);
            return dataObjHolder;
        }

        @Override
        public void onBindViewHolder(MyAdpterTacking2.ViewHolder holder, int position) {
            holder.title.setText(mDataset.get(position));

        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }




        public void setOnItemClickListener(MyAdpterTacking2.MyClickListener mCallback){
            this.mCallback = mCallback;
        }

    public MyAdpterTacking2(List<String> myDataset) {
            mDataset = myDataset;
        }

        public interface MyClickListener{
            public void onItemClick(int position, View v);

        }

        public class ViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener{
            TextView title;


            public ViewHolder(View itemView) {
                super(itemView);
                title = (TextView)itemView.findViewById(R.id.name);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {

                mCallback.onItemClick(getAdapterPosition(), v);
            }
        }
}
