package com.example.appnew;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class Myfirebase extends AsyncTask<String,Void,String> {

    Context ctx;
    MessageListener2 mListener;

    public Myfirebase( ) {

    }
    public Myfirebase(Context ctx) {
        this.ctx=ctx;
    }


    @Override
    protected String doInBackground(String... params) {

        String data = "";

        HttpURLConnection httpURLConnection = null;
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes("PostData=" + params[1]);
            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                data += current;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        mListener.messageReceivedServer(data);
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        //Log.e("TAG", result); // this is expecting a response code to be sent from your server upon receiving the POST data
        Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();


       /*if(mListener != null) {
           Log.d("bbn", result);
           mListener.messageReceivedServer(result);
       }*/
    }

    public void bindListner(MessageListener2 listener){
        mListener =listener;
    }

    public  void unBindListener(){
        mListener=null;
    }




      /* @Override
        protected String doInBackground(String... strings) {
            String urlrequest = strings[0];
            String urlparams = strings[1];

            try {

                URL url = new URL(urlrequest);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestMethod( "POST" );
                httpURLConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setUseCaches( false );
                DataOutputStream dataOutput = new DataOutputStream(httpURLConnection.getOutputStream());
                dataOutput.writeBytes(urlparams);
                dataOutput.flush();
                dataOutput.close();
                DataInputStream dataInput = new DataInputStream(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInput));
                String response = bufferedReader.readLine();
                bufferedReader.close();
                httpURLConnection.disconnect();
                return response;
            } catch (MalformedURLException e) {

            } catch (UnsupportedEncodingException e) {

            } catch (IOException e) {

            }
            return null;
        }


    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);

        Toast.makeText(ctx,result,Toast.LENGTH_LONG).show();


    }*/



}
