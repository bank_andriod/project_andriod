package com.example.appnew;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MyAdapterImg extends RecyclerView.Adapter<MyAdapterImg.ViewHolder>{


    private List<DataImg> mDataset;
    private MyClickListener mCallback;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_img,parent,false);

        ViewHolder dataObjHolder = new ViewHolder(view);
        return dataObjHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(mDataset.get(position).titel);
        holder.price.setText("฿"+mDataset.get(position).price);
        Picasso.get().load(mDataset.get(position).img)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public void setOnItemClickListener(MyClickListener mCallback){
        this.mCallback = mCallback;
    }

    public MyAdapterImg(List<DataImg> myDataset) {
        mDataset = myDataset;
    }

    public interface MyClickListener{
        public void onItemClick(int position, View v);

    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView title,price;
        ImageView imageView;
       /* ImageView icon;*/

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            price=(TextView)itemView.findViewById(R.id.price);
            imageView = (ImageView) itemView.findViewById(R.id.thumbnail);
            //icon = (ImageView)itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            mCallback.onItemClick(getAdapterPosition(), v);
        }
    }

}
