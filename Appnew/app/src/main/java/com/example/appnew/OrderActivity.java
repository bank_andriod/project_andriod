package com.example.appnew;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.appnew.ui.listtrcking.ListTrckingFragment;
import com.example.appnew.ui.orderfragment2.OrderFragment2;
import com.example.appnew.ui.waittransportfragment2.WaitTransportFragment2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class OrderActivity extends AppCompatActivity implements WaitTransportFragment2.MyFragmentListener{

    private MyAdapterOrderCust mAdapter;
    private RecyclerView recyclerView;
    private List<DataImg> datas = new ArrayList<>();
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference myRef2;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    ChildEventListener childEventListener;
    String uid="";
    WaitTransportFragment2 Event;
    Fragment MyFragment;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        Fragment selectedFragment1 = null;
        Fragment selectedFragment2 = null;
        Fragment selectedFragment3 = null;
        Fragment selectedFragment4 = null;

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_wait_payment:
                        selectedFragment1 = OrderFragment2.newInstance();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, selectedFragment1,"MY_FRAGMENT").commit();
                    return true;

                case R.id.navigation_wait_transport:
                        selectedFragment2 = WaitTransportFragment2.newInstance();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, selectedFragment2,"23").commit();

                    return true;
                case R.id.navigation_process_transport:

                   // selectedFragment = WaitTransportFragment2.newInstance();
                    //Fragment fragment = getFragmentManager().findFragmentByTag("MY_FRAGMENT");

                    return true;
                case R.id.navigation_transport_succe:

                    return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, new OrderFragment2())
                    .commit();
        }

        /*mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        user = mAuth.getCurrentUser();
        if (user != null) {
            uid = user.getUid();
        }
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapterOrderCust(datas);
        recyclerView.setAdapter(mAdapter);*/

       // myRef = database.getReference("wait_for_payment");

       /* myRef.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Order model1 = dataSnapshot.getValue(Order.class);
                String keydetail = model1.keydetail;

                myRef2 = database.getReference("imgpubilc").child("-LdqyyS3BHXhY5h117fc");
                myRef2.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        DataImg model2 = dataSnapshot.getValue(DataImg.class);
                        model2.key = dataSnapshot.getKey();
                        datas.add(model2);
                        recyclerView.scrollToPosition(datas.size() - 1);
                        mAdapter.notifyItemInserted(datas.size() - 1);
                        Log.d("imgdata", dataSnapshot.toString());

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        Toast.makeText(OrderActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
                //getdata();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Toast.makeText(OrderActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });*/

      //  getdata();



    }



    public void  getdata(){

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        Order model1 = dataSnapshot.getValue(Order.class);
                        String keydetail = model1.keydetail;
                        Log.d("key", keydetail);
                        myRef2 = database.getReference("imgpubilc");
                        myRef2.child(keydetail).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot2) {
                                DataImg model2 = dataSnapshot2.getValue(DataImg.class);
                                model2.key = dataSnapshot2.getKey();
                                datas.add(model2);
                                recyclerView.scrollToPosition(datas.size() - 1);
                                mAdapter.notifyItemInserted(datas.size() - 1);
                                Log.d("imgdata", dataSnapshot2.toString());

                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                Toast.makeText(OrderActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.set(i,model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i,model);
                            }
                        }

                        Log.d("img" , dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }

                        Log.d("img" , dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef = database.getReference("wait_for_payment");
        myRef.child(uid).addChildEventListener(childEventListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }

    @Override
    public void someEvent(OrderDetail a, DataImg b) {

        WaitTransportFragment2 Event = WaitTransportFragment2.newInstance();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.contentContainer, Event);
        transaction.commit();

        Event.doSomethingByActivity();

    }

  /*  @Override
    protected void onResume() {
        super.onResume();

        Event.CallBackFragment(new FragmentCallback() {

            @Override
            public void someEvent(OrderDetail a, DataImg b) {

            }
        });

    }*/



}
