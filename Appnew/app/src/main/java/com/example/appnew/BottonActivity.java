package com.example.appnew;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BottonActivity extends AppCompatActivity {
    DatabaseReference myRef;
    ChildEventListener childEventListener;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    String uid;
    String permission;
    private MyAdapterImg mAdapter;
    private RecyclerView recyclerView;
    private List<DataImg> datas = new ArrayList<>();
    ImageView imageView;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String shop_name ;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intent5 = new Intent(BottonActivity.this, BottonActivity.class);
                    intent5.putExtra("permission", permission);
                    intent5.putExtra("uid", uid);
                    startActivity(intent5);

                    return true;
                case R.id.navigation_dashboard:

                    Intent intent2 = new Intent(BottonActivity.this, OrderShopActivity.class);
                    intent2.putExtra("permission", permission);
                    intent2.putExtra("uid", uid);
                    startActivity(intent2);
                    return true;
                case R.id.navigation_dashboard2:

                    Intent intent3 = new Intent(BottonActivity.this, UploadIMGActivity.class);
                    intent3.putExtra("permission", permission);
                    intent3.putExtra("uid", uid);
                    startActivity(intent3);
                    return true;

                case R.id.navigation_dashboard3:
                    Intent intent4 = new Intent(BottonActivity.this, TackingActivity.class);
                    startActivity(intent4);
                    return true;
                case R.id.navigation_notifications:
                    Intent intent = new Intent(BottonActivity.this, MainActivity.class);
                    intent.putExtra("permission", permission);
                    intent.putExtra("uid", uid);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botton);

        mTextMessage = (TextView) findViewById(R.id.message);
        imageView = (ImageView) findViewById(R.id.thumbnail);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();

        getShopName();
        getuidAndpermission();


        appBar.setTitle(shop_name);



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        myRef = database.getReference("img");

        user = mAuth.getCurrentUser();

        if (user == null) {

            Toast.makeText(getBaseContext(), "Please Sing In", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(intent);
        }

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapterImg(datas);
        recyclerView.setAdapter(mAdapter);
        getdata();
    }


    public void getdata() {

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key = dataSnapshot.getKey();
                        datas.add(model);
                        recyclerView.scrollToPosition(datas.size() - 1);
                        mAdapter.notifyItemInserted(datas.size() - 1);
                        Log.d("imgdata", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.set(i, model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i, model);
                            }
                        }

                        Log.d("img", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key = dataSnapshot.getKey();

                        for (int i = 0; i < datas.size(); i++) {
                            if (datas.get(i).key.equals(model.key)) {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }

                        Log.d("img", dataSnapshot.toString());
                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef.child(uid).addChildEventListener(childEventListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        ((MyAdapterImg) mAdapter).setOnItemClickListener(
                new MyAdapterImg.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        Intent intent = new Intent(getBaseContext(), ShowProducOwnertActivity.class);
                        intent.putExtra("photos", datas.get(position).img);
                        intent.putExtra("titel", datas.get(position).titel);
                        intent.putExtra("price", (int) datas.get(position).price);
                        intent.putExtra("stock", (int) datas.get(position).stock);
                        intent.putExtra("detail", datas.get(position).detail);
                        intent.putExtra("key", datas.get(position).key);
                        intent.putExtra("person", datas.get(position).person);
                        intent.putExtra("permission", permission);
                        intent.putExtra("uid", uid);
                        startActivity(intent);
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, 1, 1, "ร้านค้า");
        menu.add(0, 2, 2, "ดูรายการย้อนหลัง");
        menu.add(0, 3, 3, "ออกจากระบบ");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case 0:
                Toast.makeText(this, "sel 0", Toast.LENGTH_SHORT).show();
                return true;
            case 1:

                Intent itn = new Intent(this, BottonActivity.class);
                itn.putExtra("permission", permission);
                itn.putExtra("uid", uid);
                startActivity(itn);
                return true;
            case 2:
                Intent itn2 = new Intent(this, MainActivity.class);
                itn2.putExtra("permission", permission);
                itn2.putExtra("uid", uid);
                startActivity(itn2);

                return true;
            case 3:
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(this, "ออกจากระบบเรียบร้อย", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(BottonActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void getuidAndpermission() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            uid = bundle.getString("uid");
            permission = bundle.getString("permission");
        }

    }

    public void getShopName() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
            shop_name = prefs.getString("shopname", "No name defined");//"No name defined" is the default value.
        }

    }


}
