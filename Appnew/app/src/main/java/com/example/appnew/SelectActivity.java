package com.example.appnew;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class SelectActivity extends AppCompatActivity {

    TextView title,showprice,showstock,showdetail,showName;
    ImageView imageView;
    String prikey;
    FirebaseUser user;
    String uid;
    int stock = 0;
    int price = 0;
    String photos;
    String titel;
    String detail;
    String shopid;
    Bundle bundle;
    private FirebaseAuth mAuth;
    DatabaseReference myRef;
    FirebaseDatabase database;
    String shop_name;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //insertbasket();
                    return true;
                case R.id.navigation_dashboard:
                    if(stock > 0){
                        insertbasket();
                    }else {
                        Toast.makeText(SelectActivity.this,"ไม่มีของใน stock", Toast.LENGTH_LONG).show();
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if(user != null) {

            uid = user.getUid();
        }else {
          /*Toast.makeText(MainActivity.this, "Please Sing In",Toast.LENGTH_LONG).show();
          Intent intent = new Intent(MainActivity.this, LoginActivity.class);
          startActivity(intent);*/
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        imageView = (ImageView) findViewById(R.id.imgView);
        title = findViewById(R.id.title);
        showprice = findViewById(R.id.price);
        showstock = findViewById(R.id.stock);
        showdetail= findViewById(R.id.detail);
        showName = findViewById(R.id.shop);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            photos = bundle.getString("photos");
            titel = bundle.getString("titel");
            price = bundle.getInt("price");
            stock = bundle.getInt("stock");
            detail = bundle.getString("detail");
            prikey = bundle.getString("key");
            shopid = bundle.getString("shopid");
            shop_name = bundle.getString("shop_name");

            Picasso.get().load(photos)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);
            title.setText(titel);
            showprice.setText("฿ "+price);
            showstock.setText("stock : "+stock);
            showdetail.setText(detail);
            Toast.makeText(SelectActivity.this, ""+shop_name, Toast.LENGTH_LONG);
            //showName.setText("ชื่อร้าน  :");


        }



    }

    public void  insertbasket()
    {
        myRef = database.getReference("basket");
        myRef.child(uid).push().setValue(prikey, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Toast.makeText(SelectActivity.this,"ใส่ตะกร้าเรียบร้อย", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void  Wait_for_payment() {
        Order order = new Order(uid,price,shopid,prikey);
        myRef = database.getReference("wait_for_payment").child(uid);
        DatabaseReference newPostRef = myRef.push();
        String KeyOrder = newPostRef.getKey();
        newPostRef.setValue(order, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Toast.makeText(SelectActivity.this,"รอการชำระเงิน", Toast.LENGTH_LONG).show();
                }
            }
        });
        Intent intent = new Intent(getBaseContext(), LocationActivity.class);
        intent.putExtra("keyOrder", KeyOrder);
        intent.putExtra("keypri", prikey);
        intent.putExtra("price", price);
        intent.putExtra("shopid", shopid);
        intent.putExtra("photos", photos);
        startActivity(intent);


    }






}
