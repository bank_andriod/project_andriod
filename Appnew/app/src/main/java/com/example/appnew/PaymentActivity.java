package com.example.appnew;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PaymentActivity extends AppCompatActivity  {
    DatabaseReference myRef;
    ChildEventListener childEventListener;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    String uid,keyOrder,keypri,shopId,photos;
    long pricevalue;
    FirebaseDatabase database;
    Myfirebase msg;
    private Button mSumit;
    private EditText date;
    private EditText time;
    private TextView price,price1,bank_no,bank_name,tel,shop_name;
    String name,location,location2,telcust;
    private Context mContext;
    MessageListener2 msb;
    Myfirebase conn2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mSumit= (Button) findViewById(R.id.button_login);
        date = (EditText) findViewById(R.id.date);
        time = (EditText) findViewById(R.id.time);
        price = (TextView) findViewById(R.id.price);

        shop_name = (TextView) findViewById(R.id.shopname);
        tel = (TextView) findViewById(R.id.tel);
        bank_name = (TextView) findViewById(R.id.bankno);
        bank_no = (TextView) findViewById(R.id.bankname);
        price1 = (TextView) findViewById(R.id.price1);



        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            photos = bundle.getString("photos");
            keyOrder = bundle.getString("keyOrder");
            keypri = bundle.getString("keypri");
            pricevalue = bundle.getInt("price");
            shopId= bundle.getString("shopid");

            name= bundle.getString("name");
            location= bundle.getString("location");
            location2= bundle.getString("location2");
            telcust= bundle.getString("tel");
            price.setText(pricevalue+" ฿");
        }

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if(user != null) {

            uid = user.getUid();
        }else {
          /*Toast.makeText(MainActivity.this, "Please Sing In",Toast.LENGTH_LONG).show();
          Intent intent = new Intent(MainActivity.this, LoginActivity.class);
          startActivity(intent);*/
        }

        mSumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  Date = date.getText().toString();
                String  Time = time.getText().toString();
                String  index = String.format("%s,%s,%d", Date, Time,pricevalue);
                JSONObject postData = new JSONObject();
                try {
                    postData.put("shopId", shopId);
                    postData.put("userId",uid);
                    postData.put("index",  index);
                    postData.put("keyorder", keyOrder);
                    postData.put("keydetial", keypri);

                   /* postData.put("name", name);
                    postData.put("location",location);
                    postData.put("location2",  location2);
                    postData.put("telcust", telcust);*/
                    /*conn = new Myfirebase(getBaseContext());
                    conn.execute(
                            "https://us-central1-appnew-c6092.cloudfunctions.net/getStatusOrder", postData.toString()
                    );*/
                    new Myfirebase(getBaseContext()).execute(
                            "https://us-central1-appnew-c6092.cloudfunctions.net/getStatusOrder", postData.toString()
                    );

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        getDataShop();
        LockStockOrder();

    }

    public void getDataOrder(){

    }

    public void getDataShop(){

        myRef = database.getReference("user");
        myRef.child(shopId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserManager value = dataSnapshot.getValue(UserManager.class);
                shop_name.setText("ชื่อร้าน :"+value.shop);
                tel.setText("เบอร์โทร :"+value.tel);
                bank_no.setText("เลขบัญชี :"+value.bankno);
                bank_name.setText("ชื่อบัญชี :"+value.bankname);
                price1.setText("ยอดที่ต้องชำระ :"+pricevalue+" ฿");

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Toast.makeText(PaymentActivity.this,error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    public void  LockStockOrder(){


        myRef = database.getReference("imgpubilc").child(uid).child(keypri);

        myRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutable) {
                DataImg p = mutable.getValue(DataImg.class);
                if (p == null) {
                    //Toast.makeText(PaymentActivity.this,"ไม่มีรายการ", Toast.LENGTH_LONG).show();
                    return Transaction.success(mutable);
                }
                if(p.stock > 0) {

                    p.stock = p.stock - 1;
                }
                // Set value and report transaction success
                mutable.setValue(p);
                return Transaction.success(mutable);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                if (databaseError != null) {

                   // Toast.makeText(PaymentActivity.this,databaseError.getMessage(), Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(PaymentActivity.this,"รอชำระเงิน", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

       /* new Myfirebase(getBaseContext()).bindListner(new MessageListener2() {
            @Override
            public void messageReceivedServer(String messageBody) {
                Log.d("fbf", messageBody);
                if (messageBody.contains("ok")) {
                    Intent intent = new Intent(PaymentActivity.this, ManuActivityCustomer.class);
                    startActivity(intent);
                    finish();
                }
            }
        });*/
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        myRef.child(uid).child(keypri).runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutable) {
                                DataImg p = mutable.getValue(DataImg.class);
                                if (p == null) {
                                    return Transaction.success(mutable);
                                }
                                if(p.stock >= 0) {

                                    p.stock = p.stock +1;
                                }
                                // Set value and report transaction success
                                mutable.setValue(p);
                                return Transaction.success(mutable);
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                if (databaseError != null) {
                                    databaseError.getMessage();
                                    // Log.w(TAG, databaseError.getMessage());
                                } else {
                                    //  Log.d(TAG, "Transaction successful");
                                }
                            }
                        });

                        PaymentActivity.super.onBackPressed();
                    }
                }).create().show();

    }


}
