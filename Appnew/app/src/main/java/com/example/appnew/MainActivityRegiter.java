package com.example.appnew;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivityRegiter extends AppCompatActivity {
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mLogin;
    private EditText mUsername;
    private EditText mPassword;
    private EditText bank_no;
    private EditText bank_name;
    private EditText Shopname;
    private EditText tel;
    private TextView mRegister;
    private Context mContext;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private String uid,email;
    private DatabaseReference myRef;
    public UserManager value;
    ChildEventListener childEventListener;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_regiter);
        mRegister = (Button) findViewById(R.id.button_summit);
        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);
        Shopname = (EditText) findViewById(R.id.Shopname);
        tel = (EditText) findViewById(R.id.tel);
        bank_no = (EditText) findViewById(R.id.bank_no);
        bank_name = (EditText) findViewById(R.id.bank_name);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("user");
        mAuth = FirebaseAuth.getInstance();




        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createAccount();
            }
        });
    }

    private void createAccount() {

         email = mUsername.getText().toString();
        String password = mPassword.getText().toString();

        Log.d("createAccount:", email);
        Log.d("createAccount:",  password);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getBaseContext(), "success", Toast.LENGTH_SHORT).show();

                            user = mAuth.getCurrentUser();
                            setUser();

                            Intent intent = new Intent(getBaseContext(), BottonActivity.class);
                            intent.putExtra("permission","shop_owner");
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("createUserWithEmail",""+ task.getException());
                            Toast.makeText(getBaseContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }


    private void  setUser() {

        UserManager value = new UserManager(email,user.getUid(),tel.getText().toString(),"shop_owner"
                ,Shopname.getText().toString(),bank_no.getText().toString(),bank_name.getText().toString());
        myRef.child(user.getUid()).push().setValue(value);


    }


}
