package com.example.appnew.ui.listtrcking;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.appnew.DataImg;
import com.example.appnew.MyAdapterOrderCust;
import com.example.appnew.MyAdpterTacking;
import com.example.appnew.MyAdpterTacking2;
import com.example.appnew.OrderDetail;
import com.example.appnew.R;
import com.example.appnew.ShowActivity;
import com.example.appnew.ShowTrackingActivity;
import com.example.appnew.TackingActivity;
import com.example.appnew.ui.tackingframent.TackingFramentFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListTrckingFragment extends Fragment {

    private MyAdpterTacking2 mAdapter;
    private RecyclerView recyclerView;
    private List<String> datas = new ArrayList<>();
    TackingFramentFragment.MyFragmentListener2 mListener;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    String uid="";
    ChildEventListener childEventListener;

    private ListTrckingViewModel mViewModel;

    public static ListTrckingFragment newInstance() {
        return new ListTrckingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_trcking_fragment, container, false);

        /*Toolbar myToolbar = (Toolbar) view.findViewById(R.id.mytoolbar);
        ((TackingActivity)getActivity()).setSupportActionBar(myToolbar);
        ActionBar appBar = ((TackingActivity)getActivity()).getSupportActionBar();
        appBar.setTitle("รายการติดตามสถาณะ");*/

        recyclerView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdpterTacking2(datas);
        recyclerView.setAdapter(mAdapter);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if (user != null) {
            uid = user.getUid();
        }

        getdata();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListTrckingViewModel.class);
        // TODO: Use the ViewModel
    }

    public  void  getdata() {

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                                String model2 = dataSnapshot.getValue(String.class);
                                datas.add(model2);
                                recyclerView.scrollToPosition(datas.size() - 1);
                                mAdapter.notifyItemInserted(datas.size() - 1);



                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {



                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {



                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };
        myRef = database.getReference("tracking_list");
        myRef.child(uid).addChildEventListener(childEventListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyAdpterTacking2) mAdapter).setOnItemClickListener(
                new MyAdpterTacking2.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {

                        Intent intent = new Intent(getActivity(), ShowTrackingActivity.class);

                        intent.putExtra("tracking", datas.get(position));
                        startActivity(intent);

                    }
                }
        );
    }



}
