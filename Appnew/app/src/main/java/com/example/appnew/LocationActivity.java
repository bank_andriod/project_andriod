package com.example.appnew;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class LocationActivity extends AppCompatActivity {
    String KeyOrder;
    String prikey;
    String photos;
    String shopid;
    int price;
    Bundle bundle;
    EditText name,location,location2,tel;
    Button btn_summit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        bundle = getIntent().getExtras();
        if (bundle != null) {

            photos = bundle.getString("photos");
            KeyOrder = bundle.getString("KeyOrder");
            price = bundle.getInt("price");
            prikey = bundle.getString("keypri");
            shopid = bundle.getString("shopid");

        }

        name = findViewById(R.id.name);
        location = findViewById(R.id.location);
        location2= findViewById(R.id.location2);
        tel= findViewById(R.id.tel);
        btn_summit=findViewById(R.id.button_summit);
        btn_summit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), PaymentActivity.class);
                intent.putExtra("keyOrder", KeyOrder);
                intent.putExtra("keypri", prikey);
                intent.putExtra("price", price);
                intent.putExtra("shopid", shopid);
                intent.putExtra("photos", photos);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("location", location.getText().toString());
                intent.putExtra("location2", location2.getText().toString());
                intent.putExtra("tel", tel.getText().toString());
                startActivity(intent);
            }
        });









    }
}
