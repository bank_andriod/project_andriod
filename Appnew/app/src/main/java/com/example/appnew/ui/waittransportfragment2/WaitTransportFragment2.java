package com.example.appnew.ui.waittransportfragment2;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appnew.DataImg;
import com.example.appnew.FragmentCallback;
import com.example.appnew.MessageListener;
import com.example.appnew.MyAdapterImg;
import com.example.appnew.MyAdapterOrderCust;
import com.example.appnew.Order;
import com.example.appnew.OrderDetail;
import com.example.appnew.R;
import com.example.appnew.SelectActivity;
import com.example.appnew.ShowActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WaitTransportFragment2 extends Fragment {

    private WaitTransportFragment2ViewModel mViewModel;
    private MyAdapterOrderCust mAdapter;
    private RecyclerView recyclerView;
    private List<DataImg> datas = new ArrayList<>();
    private List<OrderDetail> dataoOrder = new ArrayList<>();
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference myRef2;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    ChildEventListener childEventListener;
    String uid="";
    String keyOrder="";
    MyFragmentListener mListener;
    TextView message;


    public static WaitTransportFragment2 newInstance() {
        return new WaitTransportFragment2();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (MyFragmentListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Must implement MyFragmentListener");
        }

    }

    public WaitTransportFragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.wait_transport_fragment2_fragment, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        message= view.findViewById(R.id.message);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapterOrderCust(datas);
        recyclerView.setAdapter(mAdapter);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if (user != null) {
            uid = user.getUid();
        }

        getdata();


        return view;
    }


    public void  getdata(){

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {
                        keyOrder = dataSnapshot.getKey();
                        ArrayList<String> DataFromfirebase=new ArrayList<String>();
                        for (DataSnapshot jobSnapshot: dataSnapshot.getChildren()) {

                            String data = jobSnapshot.getValue(String.class);
                            DataFromfirebase.add(data);
                            Log.d("json", data);

                        }
                            OrderDetail parsedUser =  new OrderDetail(DataFromfirebase.get(2),DataFromfirebase.get(3),DataFromfirebase.get(5)
                                    ,DataFromfirebase.get(4),DataFromfirebase.get(1),DataFromfirebase.get(0),DataFromfirebase.get(6));
                            parsedUser.key = keyOrder;

                            dataoOrder.add(parsedUser);
                            myRef2 = database.getReference("imgpubilc");
                            myRef2.child(parsedUser.keydetail).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot2) {
                                DataImg model2 = dataSnapshot2.getValue(DataImg.class);
                                model2.key = dataSnapshot2.getKey();
                                datas.add(model2);
                                recyclerView.scrollToPosition(datas.size() - 1);
                                mAdapter.notifyItemInserted(datas.size() - 1);

                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }

                }
                else {
                    message.setText("ไม่มีรายการ");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        DataImg model = dataSnapshot.getValue(DataImg.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.set(i,model);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemChanged(i,model);
                            }
                        }

                    } catch (Exception ex) {
                        Log.d("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    try {

                        OrderDetail model = dataSnapshot.getValue(OrderDetail.class);
                        model.key=dataSnapshot.getKey();

                        for(int i =0;i<datas.size();i++)
                        {
                            if(datas.get(i).key.equals(model.key))
                            {
                                datas.remove(i);
                                recyclerView.scrollToPosition(i);
                                mAdapter.notifyItemRemoved(i);
                            }
                        }
                    } catch (Exception ex) {
                        Log.e("Exception", ex.getMessage());
                    }
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("DatabaseError", databaseError.getMessage());
            }
        };

        myRef = database.getReference("order_customer");
        myRef.child(uid).addChildEventListener(childEventListener);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyAdapterOrderCust) mAdapter).setOnItemClickListener(
                new MyAdapterOrderCust.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {

                        Intent intent = new Intent(getActivity(), ShowActivity.class);

                        intent.putExtra("photos", datas.get(position).img);
                        intent.putExtra("titel", datas.get(position).titel);
                        intent.putExtra("price", (int)datas.get(position).price);
                        intent.putExtra("bank_no", dataoOrder.get(position).bank_no);
                        intent.putExtra("custId", dataoOrder.get(position).custId);
                        intent.putExtra("date", dataoOrder.get(position).date);
                        intent.putExtra("time", dataoOrder.get(position).time);
                        intent.putExtra("keyOrder", dataoOrder.get(position).key);
                        startActivity(intent);


                        //mListener.someEvent(dataoOrder.get(position),datas.get(position));
                    }
                }
        );
    }

    public interface MyFragmentListener {

        public void someEvent(OrderDetail a,DataImg b);
    }

    public void doSomethingByActivity(){

    }

   /* public void CallBackFragment(FragmentCallback listener){
        mListener =listener;
    }*/

}
