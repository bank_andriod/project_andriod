package com.example.appnew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.appnew.ui.orderfragment2.OrderFragment2;

public class OrderFragment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, OrderFragment2.newInstance())
                    .commitNow();
        }
    }
}
