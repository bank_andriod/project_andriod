package com.example.appnew.ui.tackingframent;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appnew.DataImg;
import com.example.appnew.MainActivityRegiter;
import com.example.appnew.MessageListener2;
import com.example.appnew.MyAdapterOrderCust;
import com.example.appnew.MyAdpterTacking;
import com.example.appnew.OrderDetail;
import com.example.appnew.R;
import com.example.appnew.TackingActivity;
import com.example.appnew.ui.waittransportfragment2.WaitTransportFragment2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TackingFramentFragment extends Fragment {

    private TackingFramentViewModel mViewModel;
    private TextView mTextMessage;
    private MyAdpterTacking mAdapter;
    private RecyclerView recyclerView;
    private EditText tacking;
    private Button btn_sumit;
    private Button btn_addlist;
    private List<String> datas = new ArrayList<>();
    MyFragmentListener2 mListener;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private FirebaseAuth mAuth;
    FirebaseUser user;
    String uid="";

    public static TackingFramentFragment newInstance() {
        return new TackingFramentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tacking_frament_fragment, container, false);

       /* Toolbar myToolbar = (Toolbar) view.findViewById(R.id.mytoolbar);
        ((TackingActivity)getActivity()).setSupportActionBar(myToolbar);
        ActionBar appBar = ((TackingActivity)getActivity()).getSupportActionBar();
        appBar.setTitle("เช็คเลขพัสดุ");*/

        recyclerView = (RecyclerView)view.findViewById(R.id.my_recycler_view);
        tacking = (EditText) view.findViewById(R.id.tacking);
        btn_sumit = (Button) view.findViewById(R.id.button_summit);
        btn_addlist = (Button) view.findViewById(R.id.button_addtobot);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdpterTacking(datas);
        recyclerView.setAdapter(mAdapter);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if (user != null) {
            uid = user.getUid();
        }

        btn_sumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.someEvent(tacking.getText().toString());
            }
        });


        btn_addlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myRef= database.getReference("tracking_list");
                myRef.child(user.getUid()).push().setValue(tacking.getText().toString());
                Toast.makeText(getContext(),"เพิ่มเข้ารายการเรียบร้อย", Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TackingFramentViewModel.class);
        try {
            mListener = (MyFragmentListener2) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Must implement MyFragmentListener");
        }

        // TODO: Use the ViewModel
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MyAdpterTacking) mAdapter).setOnItemClickListener(
                new MyAdpterTacking.MyClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {

                    }
                }
        );
    }


    public  void  doSomethingByActivity(List<String> data){

        Log.d("html" , data.get(1));

        datas.clear();
        /*mAdapter.notifyDataSetChanged();*/

        for(int i = 0; i< data.size();i++){
            datas.add(data.get(i));
        }
        mAdapter.notifyDataSetChanged();
    }


    public interface MyFragmentListener2 {

        public void someEvent(String b);
    }






}
