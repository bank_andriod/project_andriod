package com.example.appnew;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivityRegiterCust extends AppCompatActivity {

    private Button mLogin;
    private EditText mUsername;
    private EditText mPassword;
    private TextView mRegister;
    private TextView mRegisterCust;
    private Context mContext;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private String uid;
    private DatabaseReference myRef;
    public UserManager value;
    ChildEventListener childEventListener;
    FirebaseDatabase database;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiter_customer);
        mContext = this;
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("user");

        mLogin = (Button) findViewById(R.id.button_login);
        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();

            }
        });


    }

    private void checkLogin() {
        email = mUsername.getText().toString();
        String password = mPassword.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("signInWithEmail", "signInWithEmail:success");
                            user = mAuth.getCurrentUser();
                            Toast.makeText(getBaseContext(), "Success", Toast.LENGTH_SHORT).show();
                            setUser();
                            UserManagerCust value = new UserManagerCust(email,user.getUid(),"customer");
                            Intent intent = new Intent(getBaseContext(), ManuActivityCustomer.class);
                            intent.putExtra("permission","customer");
                            startActivity(intent);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("Authentication failed.", ""+ task.getException());
                            Toast.makeText(getBaseContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });


    }

    private void  setUser() {


        UserManagerCust value = new UserManagerCust(email,user.getUid(),"customer");
        myRef.child(user.getUid()).setValue(value);


    }
}
