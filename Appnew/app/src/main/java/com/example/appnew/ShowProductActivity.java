package com.example.appnew;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class ShowProductActivity extends AppCompatActivity {

    TextView title,showprice,showstock,showdetail;
    ImageView imageView;
    String prikey;
    FirebaseUser user;
    String uid;
    int stock = 0;
    int price = 0;
    String photos;
    String titel;
    String detail;
    String shopid;
    Bundle bundle;
    private FirebaseAuth mAuth;
    DatabaseReference myRef;
    FirebaseDatabase database;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText("ใส่ตะกร้า");
                    insertbasket();
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText("ซื้อทันที");
                    if(stock > 0){
                        Wait_for_payment();
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        user = mAuth.getCurrentUser();

        if(user != null) {

            uid = user.getUid();
        }else {
          /*Toast.makeText(MainActivity.this, "Please Sing In",Toast.LENGTH_LONG).show();
          Intent intent = new Intent(MainActivity.this, LoginActivity.class);
          startActivity(intent);*/
        }

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        imageView = (ImageView) findViewById(R.id.imgView);
        title = findViewById(R.id.title);
        showprice = findViewById(R.id.price);
        showstock = findViewById(R.id.stock);
        showdetail= findViewById(R.id.detail);

         bundle = getIntent().getExtras();
        if (bundle != null) {
             photos = bundle.getString("photos");
             titel = bundle.getString("titel");
            price = bundle.getInt("price");
            stock = bundle.getInt("stock");
            detail = bundle.getString("detail");
            prikey = bundle.getString("key");
            shopid = bundle.getString("shopid");

            Picasso.get().load(photos)
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);
            title.setText(titel);
            showprice.setText("฿ "+price);
            showstock.setText("stock : "+stock);
            showdetail.setText(detail);

        }



    }

    public void  insertbasket()
    {
        myRef = database.getReference("basket");
        myRef.child(uid).push().setValue(prikey, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("firebase insert", "Data could not be saved " + databaseError.getMessage());

                } else {
                    Log.d("firebase insert", "Data saved successfully.");
                }
            }
        });

    }

    public void  Wait_for_payment() {
        Order order = new Order(uid,price,shopid,prikey);

        /*myRef = database.getReference("wait_for_payment").child(uid);
        DatabaseReference newPostRef = myRef.push();
        String KeyOrder = newPostRef.getKey();
        newPostRef.setValue(order);*/
        Intent intent = new Intent(getBaseContext(), PaymentActivity.class);
        //intent.putExtra("keyOrder", KeyOrder);
        intent.putExtra("keypri", prikey);
        intent.putExtra("price", price);
        finish();



    }


}
