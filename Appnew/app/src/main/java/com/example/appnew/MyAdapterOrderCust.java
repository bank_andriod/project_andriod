package com.example.appnew;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapterOrderCust extends RecyclerView.Adapter<MyAdapterOrderCust.ViewHolder> {
    private List<DataImg> mDataset;
    private MyAdapterOrderCust.MyClickListener mCallback;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order,parent,false);

        ViewHolder dataObjHolder  = new ViewHolder(view);
        return dataObjHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(mDataset.get(position).titel);
        holder.price.setText("฿"+mDataset.get(position).price);

        if(mDataset.get(position).logistics.contains("kerry")) {
            holder.imageView.setImageResource(R.drawable.kerry);
        }else {
            holder.imageView.setImageResource(R.drawable.ems);
        }


        /*Picasso.get().load(mDataset.get(position).img)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);*/

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }




    public void setOnItemClickListener(MyAdapterOrderCust.MyClickListener mCallback){
        this.mCallback = mCallback;
    }

    public MyAdapterOrderCust(List<DataImg> myDataset) {
        mDataset = myDataset;
    }

    public interface MyClickListener{
        public void onItemClick(int position, View v);

    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView title,price,status,email;
        ImageView imageView;
        /* ImageView icon;*/

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            price=(TextView)itemView.findViewById(R.id.price);
            imageView = (ImageView) itemView.findViewById(R.id.thumbnail);
            status = (TextView)itemView.findViewById(R.id.status);
            email=(TextView)itemView.findViewById(R.id.email);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            mCallback.onItemClick(getAdapterPosition(), v);
        }
    }

}
