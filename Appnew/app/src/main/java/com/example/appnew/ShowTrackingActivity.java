package com.example.appnew;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.appnew.ui.tackingframent.TackingFramentFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class ShowTrackingActivity extends AppCompatActivity {
    private MyAdpterTacking mAdapter;
    private RecyclerView recyclerView;
    private List<String> datas = new ArrayList<>();
    private String tracking=" ";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tracking);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.mytoolbar);
        setSupportActionBar(myToolbar);
        ActionBar appBar = getSupportActionBar();
        //appBar.setLogo(R.mipmap.ic_launcher);
        appBar.setTitle("สถาณะพัสดุ");

        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdpterTacking(datas);
        recyclerView.setAdapter(mAdapter);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            tracking = bundle.getString("tracking");
        }

        if(!tracking.contains(" ")){
            new MyTarcking(getBaseContext()).execute(tracking);
        }


    }


    public class MyTarcking extends AsyncTask<String,Void,String> {

        Context mContext;

        MessageListener2 mListener;

        public MyTarcking( Context mContext) {

            this.mContext=mContext;

        }



        @Override
        protected String doInBackground(String... params) {

            final StringBuilder builder = new StringBuilder();

            try {

                Document doc = Jsoup.connect("https://th.kerryexpress.com/en/track/?track="+params[0]).get();
                //Elements info = doc.select("div[class=\"info\"]");
                Elements links = doc.select("div[class=\"status normaly-waiting\"]");

                try {
                    Elements suseec = doc.select("div[class=\"status piority-success\"]");
                    for (Element suseecs : suseec) {
                        Log.d("html2", suseecs.text());
                        datas.add(suseecs.text());
                    }
                }catch (Exception e)
                {
                    Log.d("error" , e.toString());

                }

               /*  for (Element infos : info) {
                    Log.d("html2" , infos.text());
                    datas.add(infos.text());
                }*/



                for (Element link : links) {
                    Log.d("html2" , link.text());
                    datas.add(link.text());

                }



            } catch (Exception e) {
                Log.d("error" , e.toString());

            }

            return builder.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mAdapter.notifyDataSetChanged();
        }

        public void bindListner(MessageListener2 listener) {
            mListener = listener;
        }

        public void unBindListener() {
            mListener = null;
        }

    }
}
